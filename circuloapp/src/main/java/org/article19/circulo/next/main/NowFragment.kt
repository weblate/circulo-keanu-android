package org.article19.circulo.next.main

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.WrapContentLinearLayoutManager
import info.guardianproject.keanu.core.util.extensions.isReaction
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.R
import org.article19.circulo.next.main.now.People
import org.article19.circulo.next.main.now.PeopleAdapter
import org.article19.circulo.next.main.now.Status
import org.article19.circulo.next.main.updatestatus.UpdateStatusActivity
import org.matrix.android.sdk.api.session.events.model.Event
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.timeline.*
import kotlin.collections.ArrayList

class NowFragment: Fragment() {

    private val mApp: ImApp?
        get() = activity?.application as? ImApp

    private var peopleAdapter : PeopleAdapter? = null

    private var statusFromMe = true

    private var statusButton : Button? = null

    private var timeline : Timeline? = null

    private var mRoom : Room? = null

    private val peopleList = ArrayList<People>()

    private lateinit var recyclerViewPeople : RecyclerView

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_now, container, false)
        val layoutEmptyState = rootView.findViewById<RelativeLayout>(R.id.layout_empty_state)
        recyclerViewPeople = rootView.findViewById<RecyclerView>(R.id.recycler_view_people)
        val llm = WrapContentLinearLayoutManager(context)
        recyclerViewPeople.layoutManager = llm

        statusButton = rootView.findViewById<Button>(R.id.btn_post_feed)

        val displayName = mApp?.matrixSession?.getUser(mApp?.matrixSession?.myUserId!!)?.displayName
        rootView.findViewById<TextView>(R.id.tv_now_greetings).text = "${getString(R.string.hello)} $displayName"

        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN)
        val queryParams = builder.build()

        val listRooms = mApp?.matrixSession?.getRoomSummaries(queryParams)
        val inAcircle = listRooms?.isEmpty() == false

        if (!inAcircle) {
            layoutEmptyState.visibility = View.VISIBLE
            rootView.findViewById<TextView>(R.id.tv_people_title).visibility = View.GONE
            recyclerViewPeople.visibility = View.GONE
            statusButton?.visibility = View.GONE

        } else {
            layoutEmptyState.visibility = View.GONE
            recyclerViewPeople.visibility = View.VISIBLE

            if (peopleAdapter == null) {
                val peopleList = ArrayList<People>()
                peopleAdapter = PeopleAdapter(requireActivity(), peopleList)

                val roomId = listRooms?.get(0)?.roomId
                mRoom = roomId?.let { mApp?.matrixSession?.getRoom(it) }

            }

            recyclerViewPeople.adapter = peopleAdapter

        }

        recyclerViewPeople.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if (!statusFromMe) {
                    if (dy > 0) {
                        statusButton?.visibility = View.GONE
                    } else {
                        statusButton?.visibility = View.VISIBLE
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        statusButton?.setOnClickListener {
            startActivity(Intent(context, UpdateStatusActivity::class.java))
        }


        return rootView
    }

    override fun onPause() {
        super.onPause()
        mApp?.currentForegroundRoom = ""

        timeline?.dispose()

    }

    override fun onResume() {
        super.onResume()
        if (mRoom != null) {
            mApp?.currentForegroundRoom = mRoom?.roomId.toString()

            timeline = mRoom?.createTimeline(null, TimelineSettings(50, true))
            timeline?.addListener(tListener)
            timeline?.start()
            loadStatusFromTimeline()

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        timeline?.dispose()
    }

    private fun loadStatusFromTimeline () {

        peopleList.clear()
        statusFromMe = false

        var lastEventId = ""

        for (i in 0..1000)
        {
            val event = timeline?.getTimelineEventAtIndex(i)
            if (event != null) {
                addStatusFromEvent(event)
                lastEventId = event.eventId
            }
        }

        lastEventId.let {
            mCoroutineScope.launch {
                mRoom?.setReadMarker(it)
            }
        }

        peopleAdapter?.updateStatus(peopleList)

        if (statusFromMe)
            statusButton?.visibility = View.GONE
        else
            statusButton?.visibility = View.VISIBLE

        recyclerViewPeople.smoothScrollToPosition(0)

    }

    fun updateStatus (statusEvents : List<TimelineEvent>) {

        peopleList.clear()
        statusFromMe = false

        var lastEventId = ""

        for (event in statusEvents)
        {
            addStatusFromEvent(event)
            lastEventId = event.eventId
        }

        lastEventId.let {
            mCoroutineScope.launch {
                mRoom?.setReadMarker(it)
            }
        }

        peopleAdapter?.updateStatus(peopleList)

        if (statusFromMe)
            statusButton?.visibility = View.GONE
        else
            statusButton?.visibility = View.VISIBLE

        recyclerViewPeople.smoothScrollToPosition(0)

    }

    private val tListener = object : Timeline.Listener {

        override fun onNewTimelineEvents(eventIds: List<String>) {

            for (eventId in eventIds)
            {
                mCoroutineScope.launch {
                    mRoom?.setReadReceipt(eventId)
                }

            }

        }

        override fun onTimelineFailure(throwable: Throwable) {
            TODO("Not yet implemented")
        }


        override fun onTimelineUpdated(snapshot: List<TimelineEvent>) {

           updateStatus(snapshot)


        }
    }

    private val MSG_TYPE_REDACTION = "m.room.redaction"

    private fun addStatusFromEvent (event: TimelineEvent)
    {
        //TODO only show status events that are NOT replies to other messages or redacted

        if (event.isReply()||event.isReaction()||event.root.isRedacted())
            return

        if (event.root.type?.equals(MSG_TYPE_REDACTION) == true)
            return

        val messageContent = event.getLastMessageContent()

        var messageText = ""

        if (messageContent is MessageTextContent) {
            messageText = messageContent.body
        }
        else if (messageContent is MessageWithAttachmentContent)
        {
            messageText = messageContent.getFileName()
        }
        else
        {
            return
        }


        var isUrgent = false;
        var status = -1

        val commentCount = 0
        val isUnread = false
        val lastCommentTime = event.root.originServerTs!!

        if (messageText.isNotEmpty()) {
            isUrgent = messageText.contains(Status.URGENT_STRING)

            if (messageText.contains(Status.SAFE_STRING))
                status = Status.SAFE
            else if (messageText.contains(Status.NOT_SAFE_STRING))
                status = Status.NOT_SAFE
            else if (messageText.contains(Status.UNCERTAIN_STRING))
                status = Status.UNCERTAIN
        }

        if (!statusFromMe)
            statusFromMe = event.senderInfo.userId.equals(mApp?.matrixSession?.myUserId)

        peopleList.add(
            People(
                event.roomId,
                event.eventId,
                event.senderInfo.userId,
                event.senderInfo.disambiguatedDisplayName,
                status,
                commentCount,
                isUnread,
                isUrgent,
                lastCommentTime
            )
        )

    }
}


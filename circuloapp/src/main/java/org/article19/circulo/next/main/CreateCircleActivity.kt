package org.article19.circulo.next.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import info.guardianproject.keanu.core.ImApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.R
import org.article19.circulo.next.main.circleinfo.CircleInfoActivity
import org.matrix.android.sdk.api.session.room.model.RoomHistoryVisibility
import org.matrix.android.sdk.api.session.room.model.create.CreateRoomParams

class CreateCircleActivity : BaseActivity() {

    companion object {
        const val BUNDLE_EXTRA_CIRCLE_TYPE = "bundle_extra_circle_type"
    }

    private val mApp: ImApp?
        get() = getApplication() as? ImApp


    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_circle)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.findViewById<TextView>(R.id.tv_back_text).visibility = View.GONE
        toolbar.findViewById<TextView>(R.id.tv_action).visibility = View.VISIBLE
        toolbar.findViewById<TextView>(R.id.tv_action).setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }

        val roomName = findViewById<EditText>(R.id.edit_text_room_name)

        findViewById<Button>(R.id.btn_next).setOnClickListener {

            val params = CreateRoomParams()
            params.historyVisibility = RoomHistoryVisibility.INVITED
            params.enableEncryption()
            params.name = roomName.text.toString()

            mCoroutineScope.launch {
                val roomId = mApp?.matrixSession?.createRoom(params)

                lifecycleScope.launch {

                    val circleInfoIntent = Intent(this@CreateCircleActivity, CircleInfoActivity::class.java)
                    circleInfoIntent.putExtra("roomId",roomId)
                    circleInfoIntent.putExtra(BUNDLE_EXTRA_CIRCLE_TYPE, roomName.text.toString())
                    startActivity(circleInfoIntent)
                }
            }




        }
    }
}
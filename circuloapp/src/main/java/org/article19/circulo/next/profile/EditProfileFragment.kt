package org.article19.circulo.next.profile

import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import info.guardianproject.keanu.core.util.GlideUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.R
import org.article19.circulo.next.main.MainActivity
import org.article19.circulo.next.main.ProfileFragment
import org.matrix.android.sdk.api.session.content.ContentUrlResolver

class EditProfileFragment: BaseProfileFragment() {

    private var mFragmentView: View? = null
    private lateinit var mAvatarUrl: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mFragmentView = super.onCreateView(inflater, container, savedInstanceState)
        toolbar.findViewById<TextView>(R.id.tv_back_text).setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
            showNavigationViewIfNeeded()
        }

        val tvActionDone = toolbar.findViewById<TextView>(R.id.tv_action)
        tvActionDone.visibility = View.VISIBLE
        tvActionDone.setOnClickListener {

            //Updating new user display name and avatar
            coroutineScope.launch {
                val newUsername = editTextName.text.toString()
                withContext(Dispatchers.Main) {
                    tvActionDone.text = getString(R.string.updating)
                }

                app?.matrixSession?.setDisplayName(app?.matrixSession?.myUserId ?: "", newUsername)

                if (TextUtils.isEmpty(mAvatarUrl)) {
                    uploadDefaultAvatarIfNeeded()
                }

                withContext(Dispatchers.Main) {
//                    setFragmentResult(REQUEST_KEY_AVATAR_CHANGE, bundleOf(BUNDLE_KEY_IS_AVATAR_CHANGE to isAvatarChanged))
                    activity?.supportFragmentManager?.popBackStack()
                    showNavigationViewIfNeeded()
                }
            }
        }

        editTextName.setText(arguments?.getString(ProfileFragment.BUNDLE_EXTRA_DISPLAY_NAME) ?: "")
        mAvatarUrl = arguments?.getString(ProfileFragment.BUNDLE_EXTRA_AVATAR_URL) ?: ""

        app?.matrixSession?.contentUrlResolver()?.resolveThumbnail(mAvatarUrl, 512, 512, ContentUrlResolver.ThumbnailMethod.SCALE)
            ?.let {
                GlideUtils.loadImageFromUri(activity, Uri.parse(it), ivAvatar, true)
            }

        ivSelectDefaultAvatar.setOnClickListener {
            if (!TextUtils.isEmpty(mAvatarUrl)) {
                showClearAvatarWarningDialog()
            } else {
                selectDefaultAvatar()
            }
        }

        return mFragmentView
    }

    private fun showClearAvatarWarningDialog() {
        val alertDialogBuilder = AlertDialog.Builder(context)

        alertDialogBuilder
        alertDialogBuilder
            .setMessage(getString(R.string.msg_clear_current_avatar_warning))
            .setPositiveButton(android.R.string.ok) { dialog, which ->
                mAvatarUrl = ""
                selectDefaultAvatar()
            }
            .setNegativeButton(
                android.R.string.cancel) { dialog, which -> }

        alertDialogBuilder.create().show()
    }

    private fun showNavigationViewIfNeeded() {
        if (activity is MainActivity) {
            (activity as MainActivity).showNavigationView()
        }
    }
}
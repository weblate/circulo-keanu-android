package org.article19.circulo.next.main.now

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.PrettyTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.R
import org.article19.circulo.next.common.ui.RoundRectCornerImageView
import org.article19.circulo.next.main.updatestatus.StatusDetailActivity
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import java.util.*

class PeopleAdapter(private var activity: Activity, private var peopleList: List<People>) :
    RecyclerView.Adapter<PeopleAdapter.ViewHolder>() {

    private val mApp: ImApp?
        get() = activity?.getApplication() as? ImApp
    private val mSession: Session?
        get() = mApp?.matrixSession

    private lateinit var context: Context
    private lateinit var safeString: String
    private lateinit var unsafeString: String
    private lateinit var uncertainString: String

    companion object {
        const val BUNDLE_EXTRA_PERSON_STATUS = "bundle_extra_person_status"
    }


    init {
        setHasStableIds(true)
    }

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.people_row_item, parent, false)
        context = parent.context

        safeString = context.getString(R.string.safe)
        unsafeString = context.getString(R.string.not_safe)
        uncertainString = context.getString(R.string.uncertain)

        return ViewHolder(view)
    }

    fun updateStatus(newPeopleList: List<People>) {
        peopleList = newPeopleList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentPersonStatus = peopleList[position]
        holder.tvName.text = currentPersonStatus.name
        holder.tvCommentCount.text = context.resources.getQuantityString(
            R.plurals.comment_counts,
            currentPersonStatus.commentCount,
            currentPersonStatus.commentCount
        )

        mCoroutineScope.launch {
            val user = mApp?.matrixSession?.getUser(currentPersonStatus?.userId)
            if (user?.avatarUrl?.isNotEmpty() == true) {

                mSession?.contentUrlResolver()?.resolveThumbnail(
                    user.avatarUrl,
                    59,
                    59,
                    ContentUrlResolver.ThumbnailMethod.SCALE
                )
                    ?.let {
                        withContext(Dispatchers.Main) {
                            GlideUtils.loadImageFromUri(activity, Uri.parse(it), holder.ivAvatar, false)
                        }
                    }

            }
        }

        if (currentPersonStatus.isUnread) {
            holder.tvUnread.visibility = View.VISIBLE
        } else {
            holder.tvUnread.visibility = View.GONE
        }

        if (currentPersonStatus.isUrgent) {
            holder.ivUrgent.visibility = View.VISIBLE
        } else {
            holder.ivUrgent.visibility = View.GONE
        }

        /**
        holder.tvLastCommentTime.text = context.resources.getString(
            R.string.last_comment_time,
            currentPersonStatus.lastCommentTime
        )**/
        holder.tvLastCommentTime.text = PrettyTime.format(Date(currentPersonStatus.lastCommentTime))
        
        when (currentPersonStatus.status) {
            Status.SAFE -> {
                holder.tvStatus.text = safeString
                holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    AppCompatResources.getDrawable(context, R.drawable.ic_status_safe),
                    null,
                    null,
                    null
                )
            }

            Status.NOT_SAFE -> {
                holder.tvStatus.text = unsafeString
                holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    AppCompatResources.getDrawable(context, R.drawable.ic_status_not_safe),
                    null,
                    null,
                    null
                )
            }

            Status.UNCERTAIN -> {
                holder.tvStatus.text = uncertainString
                holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    AppCompatResources.getDrawable(context, R.drawable.ic_status_uncertain),
                    null,
                    null,
                    null
                )
            }

            else -> {
                holder.tvStatus.text = ""
            }
        }

        holder.rootLayout.setOnClickListener {
            val detailIntent = Intent(context, StatusDetailActivity::class.java)
            detailIntent.putExtra(BUNDLE_EXTRA_PERSON_STATUS, currentPersonStatus)
            context.startActivity(detailIntent)
        }
    }

    override fun getItemCount(): Int {
        return peopleList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rootLayout: CardView = view.findViewById(R.id.card_view_root)
        val tvName: TextView = view.findViewById(R.id.tv_name)
        val tvStatus: TextView = view.findViewById(R.id.tv_status)
        val tvCommentCount: TextView = view.findViewById(R.id.tv_comment_count)
        val tvUnread: TextView = view.findViewById(R.id.tv_unread)
        val ivUrgent: ImageView = view.findViewById(R.id.iv_urgent)
        val tvLastCommentTime: TextView = view.findViewById(R.id.tv_last_comment_time)
        val ivAvatar: RoundRectCornerImageView = view.findViewById(R.id.iv_avatar)
    }
}